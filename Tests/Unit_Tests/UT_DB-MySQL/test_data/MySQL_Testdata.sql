-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.4-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table unit-tests.ut_hse-db_1
DROP TABLE IF EXISTS `ut_hse-db_1`;
CREATE TABLE IF NOT EXISTS `ut_hse-db_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integer_type` int(11) NOT NULL DEFAULT 0,
  `double_type` double NOT NULL DEFAULT 0,
  `text` varchar(1000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Table for MySQL/MariaDB unit tests for the "HSE-DB" library.';

-- Dumping data for table unit-tests.ut_hse-db_1: ~0 rows (approximately)
/*!40000 ALTER TABLE `ut_hse-db_1` DISABLE KEYS */;
INSERT INTO `ut_hse-db_1` (`id`, `integer_type`, `double_type`, `text`) VALUES
	(1, 42, 2.71, 'Hallo Jörg!');
/*!40000 ALTER TABLE `ut_hse-db_1` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
